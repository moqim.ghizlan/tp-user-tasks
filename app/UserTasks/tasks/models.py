from django.db import models
from datetime import datetime, timedelta, date
from django.utils.html import format_html
from django.template.defaultfilters import date as django_date






class User(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=254, null=True)

    def __str__(self):
        return self.name

class Task(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="tasks")
    name = models.CharField(max_length=250)
    description = models.TextField()
    creation_date = models.DateField(auto_now_add=True)
    end_date = models.DateField()
    done = models.BooleanField(default=False)

    def __str__(self):
        return self.name
    def get_nb_tasks(self, user_id):
        return Task.objects.filter(user=user_id).count()
    class Meta:
        ordering = ['done']



        
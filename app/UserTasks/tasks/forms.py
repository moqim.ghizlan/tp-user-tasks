from django import forms
from .models import Task, User


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'description', 'end_date', 'user', 'done']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Le nom de la tâche',}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'La description de la tâche',}),
            'end_date': forms.DateInput(attrs={
                'class': 'form-control',
                'placeholder': 'La date de fin de la tâche',}),
            'user': forms.Select(attrs={
                'class': 'form-control',}),
            'done': forms.CheckboxInput(attrs={
                'class': 'form-check-input',}),
        }

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['name', 'email']
        widgets = {
            'name': forms.TextInput(attrs={
                "class": "form-control",
                "placeholder": "Nom",
                "required": True,
                "autofocus": True,
                "id": "name"}),
            'email': forms.EmailInput(attrs={
                "class": "form-control",
                "placeholder": "Email",
                "required": True,
                "id": "email"}),       
        }
from multiprocessing import context
import re
from django.shortcuts import render, redirect
from django.http import HttpResponse
from tasks.models import Task, User
from .forms import TaskForm, UserForm


def index(request):
    data = Task.objects.all()
    return render(request, 'index.html', {'data': data})


def tasks_for_user(request, id):
    if Task.objects.filter(user=id).count() > 0:
        data = Task.objects.filter(user=id)
        return render(request, 'tasks_for_user.html', {'data': data})
    return redirect('users_list')

def users_list(request):
    data = User.objects.all()
    for user in data:
        user.nb_tasks = Task.objects.filter(user=user.id).count()
    return render(request, 'users_list.html', {'data': data})




def add_task(request):
    form = TaskForm()
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    return render(request, 'add_task.html' , {'form': form})


def add_user(request):
    form = UserForm()
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('users_list')
    return render(request, 'add_user.html' , {'form': form})

def add_user_v2(request):
    if request.method == "POST":
        nom = request.POST.get('nom')
        email = request.POST.get('email')
        user = User(email=email, name=nom)
        user.save()
        return redirect('users_list')
    return redirect('add_task')

def delete_task(request, id):
    try:
        Task.objects.filter(id=id).delete()
        return redirect('index')
    except:
        pass
    return redirect('detail', id=id)


def delete_user(request, id):
    try:
        User.objects.filter(id=id).delete()
        return redirect('users_list')
    except:
        pass
    return redirect('users_list', id=id)


def edit_task(request, id):
    form = TaskForm(instance=Task.objects.get(id=id))
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = Task.objects.filter(id=id)
            task.update(
                name = form.cleaned_data['name'],
                description = form.cleaned_data['description'],
                end_date = form.cleaned_data['end_date'],
                done = form.cleaned_data['done'],
                user = form.cleaned_data['user'])
            return redirect('index')
    return render(request, 'edit_task.html' , {'form': form})


def edit_user(request, id):
    form = UserForm(instance=User.objects.get(id=id))
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = User.objects.filter(id=id)
            user.update(
                name = form.cleaned_data['name'],
                email = form.cleaned_data['email'])
            return redirect('users_list')
    return render(request, 'edit_user.html' , {'form': form})

def task_dane(request, id):
    task = Task.objects.get(id=id)
    task.done = True
    task.save()
    return redirect('index')



def new_user(name, email):
    user = User(name=name, email=email)
    user.save()

def sort_tasks(request):
    if request.method == "POST":
        type = request.POST.get('sort-type')
        match type:
            case '1':
                return redirect('index')
            case '2':
                data = Task.objects.order_by('end_date')
                return render(request, 'index.html', {'data': data})
            case '3':
                data = Task.objects.filter(done=True).order_by('-end_date')
                return render(request, 'index.html', {'data': data})
            case '4':
                data = Task.objects.filter(done=False).order_by('-end_date')
                return render(request, 'index.html', {'data': data})    
            case '5':
                #sort by id
                data = Task.objects.order_by('id')
                return render(request, 'index.html', {'data': data})
    return redirect('index')
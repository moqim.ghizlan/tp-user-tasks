from django.test import SimpleTestCase
from tasks.forms import TaskForm, UserForm
from tasks.models import User, Task



class TestForms(SimpleTestCase):

    def test_task_form_valid_data(self):
        form = UserForm(data={
            'name': 'user',
            'email': 'email@email.email',
            })
        self.assertTrue(form.is_valid())
from django.test import TestCase, Client
from django.urls import reverse
from tasks.models import Task, User
import json, os, sys


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.tasks_list = reverse('index')

        
    def test_tasks_list_GET(self):
        response = self.client.get(self.tasks_list)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')


    
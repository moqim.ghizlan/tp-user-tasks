from django.test import TestCase
from tasks.models import User, Task

class TestModels(TestCase):
    def setUp(self):
        self.user = User.objects.create( name='User', email="email@email.email")


        
    def test_user_info_on_creation(self):
        self.assertEqual(self.user.name, "User")
        self.assertEqual(self.user.email, "email@email.email")

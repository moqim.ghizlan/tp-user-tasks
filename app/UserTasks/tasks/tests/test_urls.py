from django.test import SimpleTestCase
from django.urls import reverse, resolve
from tasks.views import index, users_list, add_task, add_user, add_user_v2,\
    sort_tasks, tasks_for_user, delete_task, delete_user,\
    edit_task, edit_user, task_dane

class TestUrls(SimpleTestCase):

    def test_index_url_is_resolved(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)
    
    def test_user_list_url_is_resolved(self):
        url = reverse('users_list')
        self.assertEquals(resolve(url).func, users_list)

    def test_add_task_url_is_resolved(self):
        url = reverse('add_task')
        self.assertEquals(resolve(url).func, add_task)

    def test_add_user_url_is_resolved(self):
        url = reverse('add_user')
        self.assertEquals(resolve(url).func, add_user)
    
    def test_add_user_v2_url_is_resolved(self):
        url = reverse('add_user_v2')
        self.assertEquals(resolve(url).func, add_user_v2)
    
    def test_sort_tasks_url_is_resolved(self):
        url = reverse('sort_tasks')
        self.assertEquals(resolve(url).func, sort_tasks)
    
    def test_tasks_for_user_url_is_resolved(self):
        url = reverse('tasks_for_user', args=[1])
        self.assertEquals(resolve(url).func, tasks_for_user)
    
    def test_delete_task_url_is_resolved(self):
        url = reverse('delete_task', args=[1])
        self.assertEquals(resolve(url).func, delete_task)
    
    def test_delete_user_url_is_resolved(self):
        url = reverse('delete_user', args=[1])
        self.assertEquals(resolve(url).func, delete_user)

    def test_edit_task_url_is_resolved(self):
        url = reverse('edit_task', args=[1])
        self.assertEquals(resolve(url).func, edit_task)

    def test_edit_user_url_is_resolved(self):
        url = reverse('edit_user', args=[1])
        self.assertEquals(resolve(url).func, edit_user)

    def test_task_dane_url_is_resolved(self):
        url = reverse('task_dane', args=[1])
        self.assertEquals(resolve(url).func, task_dane)
from django.contrib import admin
from tasks.models import Task, User
# Register your models here.
class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'creation_date', 'end_date', 'user')


class Users(admin.ModelAdmin):
    list_display = ('name', 'email')

# admin.site.register(Task, User)
admin.site.register(Task, TaskAdmin)
admin.site.register(User)

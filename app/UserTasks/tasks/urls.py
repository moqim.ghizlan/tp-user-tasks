from django.urls import path
from . import views


#      route, location, name
urlpatterns = [
    path('', views.index, name='index'),
    path('users/list', views.users_list, name='users_list'),
    path('user/<int:id>/tasks', views.tasks_for_user, name='tasks_for_user'),
    path('user/add', views.add_user, name='add_user'),
    path('user/add/2', views.add_user_v2, name='add_user_v2'),
    path('user/<int:id>/delete', views.delete_user, name='delete_user'),
    path('user/<int:id>/edit', views.edit_user, name='edit_user'),
    path('task/add', views.add_task, name='add_task'),
    path('task/<int:id>/delete', views.delete_task, name='delete_task'),
    path('task/<int:id>/edit', views.edit_task, name='edit_task'),
    path('task/<int:id>/done', views.task_dane, name='task_dane'),
    path('task/sort', views.sort_tasks, name='sort_tasks'),
]
